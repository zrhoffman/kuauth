<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;

class KuAuthConfigTest extends TestCase
{

    protected function exampleConfig()
    {
        return [
            'is_test_server' => true,
            'custom_403_page' => '12',
            'prod_cas_server' => '34',
            'qa_cas_server' => '56',
            'test_cas_server' => '78',
            'cas_server_prefix' => '90',
        ];
    }

    protected function assertConfigContains($expectedConfig, $loadConfigArgument)
    {
        $resultConfig = ku_auth_load_config($loadConfigArgument);

        foreach ($expectedConfig as $configKey => $configValue) {
            $this->assertSame($configValue, $resultConfig[$configKey]);
        }
    }

    public function testLoadConfigArray()
    {
        $testConfig = $this->exampleConfig();

        $this->assertConfigContains($testConfig, $testConfig);
    }

    public function testLoadConfigFile()
    {
        $testConfig = $this->exampleConfig();

        $filename = shell_exec('mktemp');
        file_put_contents(
            $filename,
            json_encode($testConfig)
        );

        $this->assertConfigContains($testConfig, $filename);
    }

    public function testLoadConfigEnvironment()
    {
        $initialServer = $_SERVER;
        $environment =
            [
                'KU_AUTH_IS_TEST_SERVER' => 'true',
                'KU_AUTH_CUSTOM_403_PAGE' => '12',
                'KU_AUTH_PROD_CAS_SERVER' => '34',
                'KU_AUTH_QA_CAS_SERVER' => '56',
                'KU_AUTH_TEST_CAS_SERVER' => '78',
                'KU_AUTH_CAS_SERVER_PREFIX' => '90',
            ];

        $_SERVER = array_merge($_SERVER, $environment);

        $this->assertConfigContains($this->exampleConfig(), null);

        $_SERVER = $initialServer;
    }

    public function testLoadConfigVariables()
    {
        $variables = [
            'kuAuthIsTestServer' => true,
            'kuAuthIsQaServer' => 12,
            'kuAuthCustom403Page' => '12',
            'kuAuthProdCasServer' => '34',
            'kuAuthQaCasServer' => '56',
            'kuAuthTestCasServer' => '78',
            'kuAuthCasServerPrefix' => '90',
        ];

        /* this is what triggers the static $parentScope assignment*/
        $variables['kuAuthFromParentScope'] = true;

        ku_auth_load_variable_config($variables);
        $this->assertConfigContains($this->exampleConfig(), null);
    }
}