<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;

class KuAuthTest extends TestCase
{
    public function config()
    {
        return [
            'one' => 0,
            'two' => 1,
            'three' => 2,
        ];
    }

    public function kuConfig()
    {
        return [
            'ku_auth_one' => 0,
            'ku_auth_two' => 1,
            'ku_auth_three' => 2,
        ];
    }

    public function testAddPrefix()
    {
        $config = $this->config();
        $kuConfig = $this->kuConfig();

        $this->assertSame($kuConfig, ku_auth_config_add_prefix($config));
    }

    public function testRemovePrefix()
    {
        $kuConfig = $this->kuConfig();
        $config = $this->config();

        $this->assertSame($config, ku_auth_config_remove_prefix($kuConfig));
    }

    public function testPath()
    {
        $originalUri = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : null;
        $_SERVER['REQUEST_URI'] = 'a/b/c?d=2&e=3';
        $this->assertSame('a/b/c?d=2&e=3', ku_auth_path());

        $_SERVER['REQUEST_URI'] = 'a/b/c';
        $this->assertSame('a/b/c', ku_auth_path());

        if ($originalUri === null) {
            $_SERVER['REQUEST_URI'] = $originalUri;
        } else {
            unset($_SERVER['REQUEST_URI']);
        }
    }

    public function testUrl()
    {
        $testServerValues = [
            'SERVER_PORT' => 80,
            'SERVER_NAME' => 'zombo.com',
        ];

        $tests = [
            [
                'config' => [
                    'is_test_server' => true
                ],
                'assertion' => 'http://zombo.com'
            ],
            [
                'config' => [
                    'is_test_server' => false
                ],
                'assertion' => 'https://zombo.com'
            ]
        ];

        foreach ($testServerValues as $key => $testValue) {
            $originalServer[$key] = isset($_SERVER[$key])
                ? $_SERVER[$key]
                : null;

            $_SERVER[$key] = $testValue;
        }

        foreach ($tests as $test) {
            $this->assertSame(
                $test['assertion'],
                ku_auth_url($test['config'], false, false)
            );
        }

        $_SERVER['SERVER_PORT'] = 443;

        $this->assertSame(
            'https://zombo.com',
            ku_auth_url(
                [
                    'is_test_server' => false
                ],
                false,
                false
            )
        );

        foreach ($originalServer as $key => $originalValue) {
            if ($originalValue === null) {
                $_SERVER[$key] = $originalValue;
            } else {
                unset($_SERVER[$key]);
            }
        }
    }

    public function testAllowedUsers()
    {
        $uid = 'bernice';
        $allowedUsers = ['melvin', 'lavinia'];

        $this->assertSame(false, ku_auth_check_users($uid, $allowedUsers));

        $uid = 'winnifred';
        $allowedUsers = ['theodora', 'winnifred'];

        $this->assertSame(true, ku_auth_check_users($uid, $allowedUsers));
    }
}