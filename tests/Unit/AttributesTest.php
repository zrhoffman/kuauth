<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;

class AttributesTest extends TestCase
{
    public function testExtractAttributes()
    {
        /*
         * The response we get from the server will look something like this
         */
        $xmlResponse = <<<'XMLSTRING'
<cas:serviceResponse xmlns:cas='http://www.yale.edu/tp/cas'>
    <cas:authenticationSuccess>
        <cas:user>z329h467</cas:user>
        <cas:attributes>
            <cas:kuPersonAffiliation>Radio Orphan Annie's Secret Society</cas:kuPersonAffiliation>
            <cas:kuPersonAffiliation>The Mob</cas:kuPersonAffiliation>
            <cas:telephoneNumber>+1 555 867 5309</cas:telephoneNumber>
            <cas:mail>an@email.address</cas:mail>
            <cas:eduPersonAffiliation>International Man of Mystery</cas:eduPersonAffiliation>
            <cas:eduPersonAffiliation>Paladin</cas:eduPersonAffiliation>
            <cas:eduPersonAffiliation>Member</cas:eduPersonAffiliation>
            <cas:displayName>Robert Edward O. Speedwagon</cas:displayName>
            <cas:kuPersonStuId>9001</cas:kuPersonStuId>
            <cas:title>Pokemon Trainer</cas:title>
            <cas:kuPersonPrimaryAffiliation>Radio Orphan Annie's Secret Society</cas:kuPersonPrimaryAffiliation>
            <cas:kuObjectType>person</cas:kuObjectType>
            <cas:uid>k264x567</cas:uid>
            <cas:kuPersonLHrId>3263827</cas:kuPersonLHrId>
            <cas:eduPersonPrimaryAffiliation>Paladin</cas:eduPersonPrimaryAffiliation>
            <cas:isMemberOf>cn=portal-servicenowportlet_users,ou=groups,dc=ku,dc=edu</cas:isMemberOf>
            <cas:isMemberOf>cn=web-al-it-team,ou=groups,dc=ku,dc=edu</cas:isMemberOf>
            <cas:isMemberOf>cn=portal-temp-users,ou=groups,dc=ku,dc=edu</cas:isMemberOf>
            <cas:isMemberOf>cn=web-al-cms-d7-super-admin,ou=groups,dc=ku,dc=edu</cas:isMemberOf>
            <cas:isMemberOf>cn=portal-approvalsportlet_users,ou=groups,dc=ku,dc=edu</cas:isMemberOf>
            <cas:eduPersonPrincipalName>k264x567@ku.edu</cas:eduPersonPrincipalName>
            <cas:longTermAuthenticationRequestTokenUsed>false</cas:longTermAuthenticationRequestTokenUsed>
            <cas:sn>Speedwagon</cas:sn>
            <cas:isFromNewLogin>false</cas:isFromNewLogin>
            <cas:authenticationDate>2019-07-29T17:16:00.225-05:00[America/Chicago]</cas:authenticationDate>
            <cas:kuPersonAccessCardStatus>1</cas:kuPersonAccessCardStatus>
            <cas:initials>E</cas:initials>
            <cas:givenName>Robert</cas:givenName>
            <cas:kuPersonPrimaryDepartment>Ministry of Magic</cas:kuPersonPrimaryDepartment>
            <cas:credentialType>AskingNicelyCredential</cas:credentialType>
            <cas:credentialType>SneakingInsideCredential</cas:credentialType>
            <cas:postalAddress>3828 Piermont Dr NE$Albuquerque$NM$87111-3416$ $</cas:postalAddress>
            <cas:departmentNumber>01189998819991197253</cas:departmentNumber>
            </cas:attributes>
    </cas:authenticationSuccess>
</cas:serviceResponse>
XMLSTRING;

        $expectedAttributes = [
            'kuPersonAffiliation' =>
                [
                    0 => 'Radio Orphan Annie\'s Secret Society',
                    1 => 'The Mob',
                ],
            'telephoneNumber' => '+1 555 867 5309',
            'mail' => 'an@email.address',
            'eduPersonAffiliation' =>
                [
                    0 => 'International Man of Mystery',
                    1 => 'Paladin',
                    2 => 'Member',
                ],
            'displayName' => 'Robert Edward O. Speedwagon',
            'kuPersonStuId' => '9001',
            'title' => 'Pokemon Trainer',
            'kuPersonPrimaryAffiliation' => 'Radio Orphan Annie\'s Secret Society',
            'kuObjectType' => 'person',
            'uid' => 'k264x567',
            'kuPersonLHrId' => '3263827',
            'eduPersonPrimaryAffiliation' => 'Paladin',
            'isMemberOf' =>
                [
                    0 => 'cn=portal-servicenowportlet_users,ou=groups,dc=ku,dc=edu',
                    1 => 'cn=web-al-it-team,ou=groups,dc=ku,dc=edu',
                    2 => 'cn=portal-temp-users,ou=groups,dc=ku,dc=edu',
                    3 => 'cn=web-al-cms-d7-super-admin,ou=groups,dc=ku,dc=edu',
                    4 => 'cn=portal-approvalsportlet_users,ou=groups,dc=ku,dc=edu',
                ],
            'eduPersonPrincipalName' => 'k264x567@ku.edu',
            'longTermAuthenticationRequestTokenUsed' => 'false',
            'sn' => 'Speedwagon',
            'isFromNewLogin' => 'false',
            'authenticationDate' => '2019-07-29T17:16:00.225-05:00[America/Chicago]',
            'kuPersonAccessCardStatus' => '1',
            'initials' => 'E',
            'givenName' => 'Robert',
            'kuPersonPrimaryDepartment' => 'Ministry of Magic',
            'credentialType' =>
                [
                    0 => 'AskingNicelyCredential',
                    1 => 'SneakingInsideCredential',
                ],
            'postalAddress' => '3828 Piermont Dr NE$Albuquerque$NM$87111-3416$ $',
            'departmentNumber' => '01189998819991197253',
        ];

        $actualAttributes = ku_auth_extract_attributes($xmlResponse);

        $this->assertSame($expectedAttributes, $actualAttributes);
    }
}
