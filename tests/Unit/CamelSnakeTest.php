<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;

class CamelSnakeTest extends TestCase
{
    protected function snakes()
    {
        return [
            'test_one_two',
            'some_1234_number',
        ];
    }

    protected function camels()
    {
        return [
            'testOneTwo',
            'some1234Number',
        ];
    }

    public function testCamelToSnake()
    {
        $snakes = $this->snakes();
        $camels = $this->camels();

        foreach ($snakes as $index => $snake) {
            $this->assertSame($snake, camelToSnake($camels[$index]));
        }
    }

    public function testSnakeToCamel()
    {
        $camels = $this->camels();
        $snakes = $this->snakes();

        foreach ($camels as $index => $camel) {
            $this->assertSame($camel, snakeToCamel($snakes[$index]));
        }
    }
}
