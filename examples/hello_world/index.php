<?php
/*
session_start();
session_destroy();
session_write_close();
die;
*/

/*
 * Basic KU Auth example
 */

$auth_script = 'ku-auth-this-script.php';

$config = [
    'allowed_users' => ['jimmy', 'z329h467'],
];

if (!file_exists($auth_script)) {
    file_get_contents($auth_script, 'https://websupport.ku.edu/ku-auth-this-script.php');
}

$kuAuthAutoRun = false;
require_once('ku-auth-this-script.php');
ku_auth($config);
unset($_SESSION['KUAUTH']);
?>

Hello, <?php echo $_KUAUTH['givenName'] ?>!