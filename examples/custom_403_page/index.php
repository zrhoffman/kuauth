<?php

/*
 * Basic KU Auth example
 */

$auth_script = 'ku-auth-this-script.php';

if (!file_exists($auth_script)) {
    file_get_contents($auth_script, 'https://websupport.ku.edu/ku-auth-this-script.php');
}

$kuAuthCustom403Page = 'access-denied.php';
$kuAuthAllowedUsers = ['nonexistent', 'users'];

require_once('ku-auth-this-script.php');
?>

Hello, <?php echo $_KUAUTH['givenName'] ?>!