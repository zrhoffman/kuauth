<?php

/*
 * This example uses login.test.ku.edu instead of login.ku.edu. It only works
 * when you are on-campus or using the VPN.
 */

$auth_script = 'ku-auth-this-script.php';

if (!file_exists($auth_script)) {
    file_get_contents($auth_script, 'https://websupport.ku.edu/ku-auth-this-script.php');
}

$kuAuthIsTestServer = true;
require_once('ku-auth-this-script.php');
?>

<p>
    All of the possible CAS attributes are listed below. Since this is using
    login.test.ku.edu, you will only have access to some of these CAS attributes
    when you switch to login.ku.edu unless you explicitly ask for more.
</p>

<pre>$_KU_AUTH is <?php var_export($_KUAUTH) ?>
</pre>
