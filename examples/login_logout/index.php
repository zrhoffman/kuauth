<?php

/*
 * This example shows how to skip authenticating right when
 * ku-auth-this-script.php is required/included so you can run other code
 * before authenticating but still have all of your requires/includes at the
 * top of the PHP file.
 */

$auth_script = 'ku-auth-this-script.php';

if (!file_exists($auth_script)) {
    file_get_contents($auth_script, 'https://websupport.ku.edu/ku-auth-this-script.php');
}

/*
 * You must set $kuAuthAutoRun to false to keep KU Auth from auto-running when
 * you include/require it.
 */
$kuAuthAutoRun = false;
require_once('ku-auth-this-script.php');

/*
 * Only tries to log in if the "login" GET variable is equal to true, and if we
 * are not already logged in.
 */
function maybeAuthenticate()
{
    if (isset($_KUAUTH)) {
        return;
    }

    if (isset($_GET['login']) || isset($_GET['ticket'])) {
        ku_auth();
    }
}

maybeAuthenticate();

if (!isset($_KUAUTH)):
    ?>
    <p>
        Click on the link to log in.
        <br>
        <a href="?login=true">Log in</a>
    </p>
<?php
else:
    ?>
    Logged in!
<?php
endif
?>
