<?php declare(strict_types=1);

/*
 * oooo    oooo ooooo     ooo          .o.                       .   oooo
 * `888   .8P'  `888'     `8'         .888.                    .o8   `888
 *  888  d8'     888       8         .8"888.     oooo  oooo  .o888oo  888 .oo.
 *  88888[       888       8        .8' `888.    `888  `888    888    888P"Y88b
 *  888`88b.     888       8       .88ooo8888.    888   888    888    888   888
 *  888  `88b.   `88.    .8'      .8'     `888.   888   888    888 .  888   888
 * o888o  o888o    `YbodP'       o88o     o8888o  `V88V"V8P'   "888" o888o o888o
 *
 * version 2019.8.2
 * last updated 2019-08-02
 */

$kuAuthCurrentScope = get_defined_vars();
/*
 * This is the only time that we have access to the parent scope. If the
 * config is loaded from non-global variables (e.g.,
 * $kuAuthIsTestServer, $kuAuthCustom403Page, etc.), we can grab
 * them here.
 */
$kuAuthCurrentScope['kuAuthFromParentScope'] = true;

ku_auth_load_variable_config($kuAuthCurrentScope);

unset($kuAuthFromParentScope);
if (
    ku_auth(
        [
            'auto_running' => true,
        ]
    ) !== null
) {
    global $_KUAUTH;
    $_KUAUTH = $_SESSION['KUAUTH'];
};
unset($kuAuthCurrentScope);

/*
 * Returns true if ku_auth() can proceed.
 */
function ku_auth_auto_check(array $config): bool
{
    return !($config['auto_running'] === true && $config['auto_run'] === false);
}

/**
 * @param mixed|null $passedConfig This can be an array or a filename of a JSON file
 * containing the config. Default filename ku_auth.json is checked by default,
 * but if a filename is passed to ku_auth(), then that file is loaded instead.
 * @return array
 * @throws Exception
 */
function ku_auth($passedConfig = null): ?array
{
    $kuAuthConfig = ku_auth_load_config($passedConfig);

    /*
     * We do not want to auto run if it is disabled
     */
    if (!ku_auth_auto_check($kuAuthConfig)) {
        return null;
    }

    global $_KUAUTH;
    if (session_status() !== PHP_SESSION_ACTIVE) {
        session_start();
    }

    $_KUAUTH = isset($_SESSION['KUAUTH'])
        ? $_SESSION['KUAUTH']
        : null;

    if (!is_array($_KUAUTH)) {
        try {
            $_KUAUTH = ku_auth_authenticate($kuAuthConfig);
            if (
                is_array($kuAuthConfig['allowed_users']) &&
                ku_auth_check_users(
                    $_KUAUTH['uid'],
                    $kuAuthConfig['allowed_users']
                ) !== true
            ) {
                throw new Exception('User is not allowed.');
            }
        } catch (Exception $exception) {
            if (is_string($kuAuthConfig['custom_403_page'])) {
                header(
                    sprintf(
                        'Location: %s',
                        $kuAuthConfig['custom_403_page']
                    )
                );

                die;
            } else {
                header('HTTP/2 403 '); ?>
                <!doctype html>
                <html>
                <head>
                    <title>403 Forbidden</title>
                </head>
                <body>
                <h1>Forbidden</h1>
                <p>You don't have permission to
                    access <?php echo ku_auth_path() ?>
                    on this server.</p>
                <p>Additionally, a 403 Forbidden
                    error was encountered while trying to use an ErrorDocument
                    to handle the request.</p>
                </body>
                </html>
                <?php
            }
            throw $exception;
        }
    }

    $_SESSION['KUAUTH'] = $_KUAUTH;
    ku_auth_redirect_to_auth_page($kuAuthConfig);

    return $_KUAUTH;
}

/**
 * @param mixed|null $passedConfig This can be an array or a filename of a file
 * containing the config.
 * @return array
 * @throws Exception
 */
function ku_auth_load_config($passedConfig = null): array
{
    $configTypes = [
        'is_test_server' => 'bool',
        'is_qa_server' => 'bool',
        'custom_403_page' => 'string',
        'prod_cas_server' => 'string',
        'qa_cas_server' => 'string',
        'test_cas_server' => 'string',
        'cas_server_prefix' => 'string',
        'allowed_users' => 'array',
        'auto_run' => 'bool',
        'auto_running' => 'bool',
    ];


    $defaultConfig = ku_auth_config_add_prefix(
        [
            'is_test_server' => false,
            'is_qa_server' => false,
            'custom_403_page' => null,
            'prod_cas_server' => 'login.ku.edu',
            'qa_cas_server' => 'login.qa.ku.edu',
            'test_cas_server' => 'login.test.ku.edu',
            'cas_server_prefix' => '/cas',
            'allowed_users' => null,
            'auto_run' => true, /* whether auto run is allowed */
            'auto_running' => false, /* whether it is currently auto running */
        ]
    );

    $loadedConfig = null;
    if (is_array($passedConfig)) {
        $loadedConfig = $passedConfig;
    } elseif (is_string($passedConfig) && file_exists($passedConfig)) {
        $configFile = $passedConfig;
        $loadedConfig = json_decode(
            file_get_contents(
                $passedConfig
            ),
            true
        );
    } elseif (file_exists(__DIR__ . '/ku_auth.json')) {
        $configFile = __DIR__ . '/ku_auth.json';
        $loadedConfig = json_decode(
            file_get_contents(
                __DIR__ . '/ku_auth.json'
            ),
            true
        );
    }

    if (isset($configFile) && !is_array($loadedConfig)) {
        throw new Exception(
            sprintf(
                'Error parsing %s as JSON. Please make sure the syntax for %s is correct.',
                $configFile,
                $configFile
            )
        );
    }

    if (!is_array($loadedConfig)) {
        $loadedConfig = [];
    }

    $kuAuthConfig = ku_auth_config_remove_prefix(
        array_merge(
            $defaultConfig,
            ku_auth_config_add_prefix($loadedConfig),
            ku_auth_load_environment_config($defaultConfig, $configTypes),
            ku_auth_load_variable_config($defaultConfig)
        )
    );

    if (!isset($kuAuthConfig['cas_server'])) {
        switch (true) {
            case $kuAuthConfig['is_test_server']:
                $casServerIndex = 'test_cas_server';
                break;
            case $kuAuthConfig['is_qa_server']:
                $casServerIndex = 'qa_cas_server';
                break;
            default:
                $casServerIndex = 'prod_cas_server';
                break;
        }

        $kuAuthConfig['cas_server'] = $kuAuthConfig[$casServerIndex];
    }

    return $kuAuthConfig;
}

/**
 * @param array $config
 * @return array
 */
function ku_auth_config_add_prefix(array $config): array
{
    return array_combine(
        array_map(
        /**
         * @param string $configName
         * @return string
         */ function (string $configName): string {
            return 'ku_auth_' . $configName;
        },
            array_keys($config)
        ),
        $config
    );
}

/**
 * @param mixed $prefixedConfig Array of configuration options or a single option name
 * @return array|false|string
 */
function ku_auth_config_remove_prefix($prefixedConfig)
{
    $unprefixFunction =
        function (string $configName): string {
            return preg_replace(
                '/^ku_auth_(.*)/',
                '\1',
                $configName
            );
        };

    return is_array($prefixedConfig)
        ? array_combine(
            array_map(
                $unprefixFunction,
                array_keys($prefixedConfig)
            ),
            $prefixedConfig
        )
        : $unprefixFunction($prefixedConfig);
}

/**
 * @param string $snake
 * @return string
 */
function snakeToCamel(string $snake): string
{
    return lcfirst(
        str_replace(
            ' ',
            '',
            ucwords(
                str_replace('_', ' ', $snake)
            )
        )
    );
}

/**
 * @param string $camel
 * @return string
 */
function camelToSnake(string $camel): string
{
    preg_match_all(
        '!([A-Z0-9][A-Z]*(?=$|[A-Z0-9][a-z])|[A-Za-z][a-z]+|[0-9]+)!',
        $camel,
        $matches
    );

    return implode(
        '_',
        array_map(
        /**
         * @param string $match
         * @return string
         */ function (string $match): string {
                return $match === strtoupper($match)
                    ? strtolower($match)
                    : lcfirst($match);
            },
            $matches[0]
        )
    );
}

/**
 * @param array $config
 * @return array
 */
function ku_auth_load_variable_config(array $config): array
{
    /* static in a function means it is only initialized once but still exists
     * other times the function is called
     */
    static $parentScope = [];
    if (isset($config['kuAuthFromParentScope']) && $config['kuAuthFromParentScope']) {
        unset($config['kuAuthFromParentScope']);
        $parentScope = $config;
        return $parentScope;
    }
    $variableConfig = array_reduce(
    /**
     * @param array $variableConfig
     * @param string $key
     * @return array
     */ array_map(
            'snakeToCamel',
            array_keys($config)
        ),
        function (array $variableConfig, string $key) use ($parentScope): array {
            $value = isset($parentScope[$key])
                ? $parentScope[$key]
                : null;

            if ($value !== null) {
                $variableConfig[$key] = $value;
            }

            return $variableConfig;
        },
        []
    );

    return array_combine(
        array_map(
            'camelToSnake',
            array_keys($variableConfig)
        ),
        $variableConfig
    );
}

/**
 * @param $maybeBool
 * @return bool
 */
function ku_auth_boolval($maybeBool): bool
{
    return filter_var($maybeBool, FILTER_VALIDATE_BOOLEAN);
}

/**
 * @param $maybeString
 * @return string
 */
function ku_auth_stringval($maybeString): string
{
    return strval($maybeString);
}

/**
 * @param $maybeArray
 * @return array
 * @throws Exception
 */
function ku_auth_arrayval($maybeArray): array
{
    $array = [];
    if (is_string($maybeArray)) {
        $array = preg_split(
            '/\s+/',
            $maybeArray
        );
    }

    if (!is_array($array)) {
        throw new Exception(sprintf('Could not convert value of type %s to type %s', gettype($array), 'array'));
    }

    $array = array_filter(
    /**
     * @param $element
     * @return bool
     */ $array,
        function ($element): bool {
            return is_string($element) && strlen($element) > 0;
        }
    );

    return $array;
}

/**
 * @param array $config
 * @param array $configTypes
 * @return array
 */
function ku_auth_load_environment_config(array $config, array $configTypes): array
{
    $variableConfig = array_reduce(
    /**
     * @param array $variableConfig
     * @param string $key
     * @return array
     */ array_map(
            'strtoupper',
            array_keys($config)
        ),
        function (array $variableConfig, string $key) use ($configTypes) {
            if (isset($_SERVER[$key])) {
                $lowerKey = strtolower($key);
                $typecastFunctionName = 'ku_auth_' . $configTypes[ku_auth_config_remove_prefix(
                        $lowerKey
                    )] . 'val';
                $variableConfig[$lowerKey] = $typecastFunctionName($_SERVER[$key]);
            }

            return $variableConfig;
        },
        []
    );

    return $variableConfig;
}

/**
 * @param string $xmlResponse
 * @return array
 */
function ku_auth_extract_attributes(string $xmlResponse): array
{
    $serviceValidationResponse = new DOMDocument();
    $serviceValidationResponse->loadXML($xmlResponse);
    assert($serviceValidationResponse->getElementsByTagName('authenticationSuccess')->item(0) instanceof DOMElement);

    $attributesNode = $serviceValidationResponse->getElementsByTagName('attributes')->item(0);
    assert($attributesNode instanceof DOMElement);

    $casAttributes = [];
    foreach ($attributesNode->childNodes as $attributeNode) {
        if (!$attributeNode instanceof DOMElement) {
            continue;
        }
        $casAttributes[$attributeNode->localName][] = $attributeNode->nodeValue;
    }

    $casAttributes = array_map(
    /**
     * @param array $attributesOfType
     * @return array|mixed
     */ function (array $attributesOfType) {
            return count($attributesOfType) === 1
                ? current($attributesOfType)
                : $attributesOfType;
        },
        $casAttributes
    );

    return $casAttributes;
}

/**
 * @param array $kuAuthConfig
 * @return array
 */
function ku_auth_authenticate(array $kuAuthConfig): array
{
    $ticket = isset($_GET['ticket'])
        ? $_GET['ticket']
        : null;

    if ($ticket === null) {
        header(
            sprintf(
                'Location: https://%s%s/login?service=%s',
                $kuAuthConfig['cas_server'],
                $kuAuthConfig['cas_server_prefix'],
                ku_auth_url($kuAuthConfig)
            )
        );
        die();
    }

    return ku_auth_extract_attributes(
        file_get_contents(
            sprintf(
                'https://%s%s/p3/serviceValidate?service=%s&ticket=%s',
                $kuAuthConfig['cas_server'],
                $kuAuthConfig['cas_server_prefix'],
                urlencode(
                    ku_auth_url($kuAuthConfig, true)
                ),
                urlencode(
                    $ticket
                )
            )
        )
    );
}

/**
 * @param $uid
 * @param array $allowedUsers
 * @return bool
 */
function ku_auth_check_users($uid, array $allowedUsers): bool
{
    return array_search($uid, $allowedUsers, true) !== false;
}

/**
 * @param bool $unsetTicket
 * @return string
 */
function ku_auth_path(bool $unsetTicket = false): ?string
{
    $uri = $_SERVER['REQUEST_URI'];
    if ($unsetTicket) {
        $parsedURI = parse_url($uri);
        $query = [];
        if (isset($parsedURI['query'])) {
            parse_str($parsedURI['query'], $query);
            unset($query['ticket']);
        }

        $uri = $parsedURI['path'];

        if (count($query) > 0) {
            $uri .= sprintf(
                '?%s',
                http_build_query($query)
            );
        }
    }

    return $uri;
}

/**
 * @param array $config
 * @param bool $unsetTicket
 * @param bool $static
 * @return string
 */
function ku_auth_url(array $config, bool $unsetTicket = false, bool $static = true): string
{
    /*
     * Cache the result, since it will not change between the start and finish
     * of a single script execution
     */
    if ($static === true) {
        static $authUrls, $authUrlIndices;
    }

    if (!isset($authUrls)) {
        $authUrls = [
            'ignoreTicket' => null,
            'unsetTicket' => null,
        ];
        $authUrlIndices = [
            false => 'ignoreTicket',
            true => 'unsetTicket',
        ];
    }

    /*
     * Mutable reference to the static array element, even if it is null
     */
    $authUrl = &$authUrls[$authUrlIndices[$unsetTicket]];

    if (isset($authUrl) && is_string($authUrl)) {
        return $authUrl;
    }

    $scheme = !$config['is_test_server']
        ? 'https'
        : [
            '80' => 'http',
            '443' => 'https',
        ][$_SERVER['SERVER_PORT']];

    $authUrl = sprintf(
        '%s://%s%s',
        $scheme,
        $_SERVER['SERVER_NAME'],
        ku_auth_path($unsetTicket)
    );

    return $authUrl;
}

/**
 * @param array $config
 *
 * @return void or terminates execution
 */
function ku_auth_redirect_to_auth_page(array $config)
{
    $kuAuthUrlNoTicket = ku_auth_url($config, true);
    $kuAuthUrl = ku_auth_url($config);

    if ($kuAuthUrlNoTicket === $kuAuthUrl) {
        return;
    }

    header(
        sprintf(
            'Location: %s',
            $kuAuthUrlNoTicket
        )
    );
    die;
}
