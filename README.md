KU Auth
=======

#### Table of contents
* [Basic setup](#basic-setup)
* [Allow only specific users](#allow-only-specific-users)

What is this?
-------------
This is an alternative to Shibboleth that works on any KU server that runs PHP 5.6 or higher (tested through PHP 7.3). No external dependencies outside of the `ku-auth-this-script.php` file are required.

<a name="basic-setup">Basic Setup</a>
-----------
1. Go to https://websupport.ku.edu/sites/websupport.ku.edu/files/ku-auth-this-script.php and save it as a file named `ku-auth-this-script.php`.

2. Go to the login page for your site (e.g., [cpanel.unit.ku.edu](https://cpanel.unit.ku.edu/)) and log in with that site's credentials.

3. Click on the link named `File Manager`, go to the directory named `public_html`, and upload `ku-auth-this-script.php`.

4. In `index.php` on your site, add the following line at the very top of the file:
```php
<?php require_once 'ku-auth-this-script.php'; ?>
```

That's it! Now, users are required to log in through single sign-on (SSO) in order to use the site. If there are any other pages, add that line in step 4 to the very top of the scripts for those pages, too.

<a name="allow-only-specific-users">Allow only specific users</a>
-------------

If you only want some users to be able to access the site, make a new file in `public_html` named `.htaccess` and add the following line (substitute your username for `c567l966`):

```apache
SetEnv KU_AUTH_ALLOWED_USERS c567l966
```

For example, if your site used to use Shibboleth and you have the following line in your `.htaccess` to allow certain users,
```apache
require user example@ku.edu anotherusername@ku.edu etcetera@ku.edu
```

That line may be rewritten for KU Auth as follows:

```apache
SetEnv KU_AUTH_ALLOWED_USERS example anotherusername etcetera
```

Keep in mind that these need to be the actual usernames, not just email addresses. You can use [directory.ku.edu](https://directory.ku.edu/) to see what the usernames for different users are. For example, if you go to [https://directory.ku.edu/directory/details?details=c567l966](https://directory.ku.edu/directory/details?details=c567l966) , the email address is clejuez@ku.edu but the username is c567l966.
